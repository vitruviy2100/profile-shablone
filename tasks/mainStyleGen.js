const {
	src,
	dest
} = require('gulp');
const sass = require('gulp-sass');
const bulk = require('gulp-sass-bulk-importer');
const concat = require('gulp-concat');

module.exports = function style() {
	return src('src/scss/main-style.scss')
		.pipe(bulk())
		.pipe(sass({
			outputStyle: 'expanded'
		}).on('error', sass.logError))
		.pipe(concat('main-style.css'))
		.pipe(dest('build/css/'))
}